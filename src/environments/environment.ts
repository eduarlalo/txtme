// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyCyuuD4vhtXHIH4Hts2OF1Vt4dv1QID_aA',
    authDomain: 'xtme-29f00.firebaseapp.com',
    databaseURL: 'https://txtme-29f00.firebaseio.com',
    projectId: 'txtme-29f00',
    storageBucket: 'txtme-29f00.appspot.com',
    messagingSenderId: '469282954095'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
