import { Component, OnInit } from '@angular/core';
import { User } from 'firebase';
import { UserService } from '../services/user.service';
import { AuthenticationService } from '../services/authentication.service';

import { AngularFireStorage } from '@angular/fire/storage';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  user: User;

  imageChangedEvent: any = '';
  croppedImage: any = '';
  visual: number;

  picture: any;


  constructor( private _us: UserService,
               private _as: AuthenticationService,
               private _fs: AngularFireStorage) {


    }

  ngOnInit() {
  }

  guardar_cambios() {
    this.visual = 0;
    if (this.croppedImage) {
      const currentimageid = this._us.user.uid;
      const pictures = this._fs.ref( 'pictures/' + currentimageid + '.png' )
          .putString( this.croppedImage, 'data_url' );
          // con el putString lo convierte en imagen binaria
          pictures.then((resp) => {
              this.picture = this._fs.ref( 'pictures/' + currentimageid + '.png' )
              .getDownloadURL();
              // devuelve un objeto, por eso nos subscribimos en la siguiente linea
              this.picture.subscribe((p) => {
                this._us.setAvatar(p, this._us.user.uid)
                  .then(() => {
                    alert('Avatar subido correctamente!');

                  }).catch((error) => {
                    alert('Ocurrio un error al subir una imagen.' + error);
                  });
              });
            }).catch((error) => {
              console.log(error);
          });
    } else {
      this._us.editUser(this._us.user).then(() => {
        alert('Usuario modificado!');

      }, (error) => {
        alert(error);

      }).catch(() => {
        alert('Ha ocurrido un error.');
       });
      }
    }

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
    this.visual = 1;
  }
  imageCropped(event: any) {
      this.croppedImage = event.base64;
  }
  imageLoaded() {
      // show cropper
  }
  loadImageFailed() {
      // show message
  }
}
