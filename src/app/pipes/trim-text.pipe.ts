import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'trimText'
})
export class TrimTextPipe implements PipeTransform {

  transform(val, args) {
    // pipe sin uso... 
    if (args === undefined) {
      return val;
    }

    if (val.length > args) {
      return val + '- <br>';

    } else {

      return val;
    }
  }

}
