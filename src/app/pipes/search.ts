import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'search'
})

export class SearchPipe implements PipeTransform {

    public transform(value, args: string) {
        // el value it's the array that we are passing, & args are
        // the query string of friend list(la palabra de busqueda)
    if ( !value ) {
        return; // avoid empty return
    }
    if ( !args ) {
        return value; // if you not entried nothing, it's gonna show all yourfriends
    }
    args = args.toLowerCase();
    return value.filter((item) => {
        return JSON.stringify(item).toLowerCase().includes(args);
        // compare the values of the array in lowercase
    });

    }
}
