import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(public db: AngularFireAuth) {}

  loginWithEmail(email: string, pass: string) {
    return this.db.auth.signInWithEmailAndPassword(email, pass);
  }

  signinWithEmail(email: string, pass: string) {
    return this.db.auth.createUserWithEmailAndPassword(email, pass);
  }

  getStatus() {
    return this.db.authState;
  }

  logOut() {
    return this.db.auth.signOut();
  }
}
