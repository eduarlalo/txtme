import { Injectable } from '@angular/core';
// import { User } from '../interfaces/user';
import { AngularFireDatabase } from '@angular/fire/database';
import { User } from '../interfaces/user';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public user: User;

  constructor(public db: AngularFireDatabase,
              private _as: AuthenticationService) {

    this._as.getStatus().subscribe((data) => {
      this.getUserId(data.uid).valueChanges().subscribe((data2: User) => {
        this.user = data2;
        console.log(this.user);
      }
      );
    },  (error) => {
      console.log(error);
    });
  }

  getUsers() {
    return this.db.list('/users'); // path o nodo de donde se obtendrá los usuarios
    // esto devuelve un observable
  }

  getUserId( uid ) {
    return this.db.object('/users/' + uid );
  }

  createUser( user ) {
    return this.db.object('/users/' + user.uid ).set(user); // se inserta en el nodo el usuario que llega en el login
  }

  editUser( user ) {
    return this.db.object('/users/' + user.uid ).set(user); // edita el usuario
  }

  setAvatar(avatar, uid) {
    return this.db.object('users/' + uid + '/avatar/' ).set(avatar);
  }

  addFriend(userId, friendId) {
    this.db.object('users/' + userId + '/friends/' + friendId ).set(friendId);
    return this.db.object('users/' + friendId + '/friends/' + userId ).set(userId);
    // con esto resolvemos la promesa y notificar que se acepto al amigo
  }


}
