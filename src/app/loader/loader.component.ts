import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';
import { AuthenticationService } from '../services/authentication.service';
import { User } from '../interfaces/user';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css']
})
export class LoaderComponent implements OnInit {

  constructor(private router: Router,
              private _us: UserService,
              private _as: AuthenticationService) {

  setTimeout(() => {
      this._as.getStatus().subscribe((data) => {
        if (data) {
          console.log('si');
          this.router.navigate(['/home']);
          location.reload(true);
        } else {
          console.log('no');
          this.router.navigate(['/login']);
        }
      }, (error) => {
        console.log(error);
      });
    }, 1000);


   }

  ngOnInit() {
    // setTimeout(() => {
    //   this.router.navigate(['login']);
    // }, 2000);
  }

}
