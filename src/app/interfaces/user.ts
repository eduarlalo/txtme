export interface User {
    nick: string;
    sub_nick?: string;
    age?: number;
    email: string;
    friend: boolean;
    uid: any;
    status?: string;
    avatar?: string;
    friends?: any;
}// ?= optionally value for the interfaces, awesome
