import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ImageCropperModule } from 'ngx-image-cropper';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { ChatComponent } from './chat/chat.component';
import { ProfileComponent } from './profile/profile.component';
import { Routes, RouterModule } from '@angular/router';
import { MenuComponent } from './menu/menu.component';
import { ChatNewComponent } from './chat-new/chat-new.component';
import { SearchPipe } from './pipes/search';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from 'src/environments/environment';

import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AuthenticationGuard } from './services/authentication.guard';
// import { TrimTextPipe } from './pipes/trim-text.pipe';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BootstrapModalModule } from 'ng2-bootstrap-modal';
import { CommonModule } from '@angular/common';
import { RequestComponent } from './modals/request/request.component';
import { ContactComponent } from './contact/contact.component';
import { LoaderComponent } from './loader/loader.component';
import { SplashComponent } from './splash/splash.component';

const appRoutes: Routes = [
  {path: '', component: LoaderComponent}, // Esto es un path anónimo...
  {path: 'home', component: HomeComponent, canActivate: [AuthenticationGuard]},
  {path: 'login', component: LoginComponent},
  {path: 'chat', component: ChatComponent},
  {path: 'chat/:uid', component: ChatNewComponent},
  {path: 'profile', component: ProfileComponent, canActivate: [AuthenticationGuard]},
];

// nota LUIS: para cambiar entre chats que sea deslizar hacia la derecha y/o izquierda, investigar como...

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    ChatComponent,
    ProfileComponent,
    MenuComponent,
    ChatNewComponent,
    SearchPipe,
    RequestComponent,
    ContactComponent,
    LoaderComponent,
    SplashComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features,
    AngularFireStorageModule,
    AngularFireDatabaseModule,
    ImageCropperModule,
    NgbModule,
    CommonModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    BootstrapModalModule.forRoot({container: document.body})
    // el lugar en donde se pondra el modal sera' en el appcomponent para que se vea en cualquier momento
  ],
  providers: [],
  bootstrap: [AppComponent],
  // entrycomponents al momento de llamarlo ya est'e disponible si no, bootstrap no lo va a encontrar
  entryComponents: [RequestComponent]
})
export class AppModule { }
