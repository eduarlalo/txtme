import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  operation: String = 'login';
  email: string = null;
  pass: string = null;
  nick: string = null;

  constructor(public _as: AuthenticationService, private _us: UserService,
              private router: Router) { }

  async login() {
    try {
      const data = await this._as.loginWithEmail(this.email, this.pass);
      alert('Iniciando sesión');
      console.log('Loggeado correctamente! = ', data);
      this.router.navigate(['/']);

    } catch (error) {
      alert('Un error ha ocurrido');
      console.log('ocurrio un error = ', error);
    }
  }

  async signin() {
    try {
      const data = await this._as.signinWithEmail(this.email, this.pass);
      const user = {
        uid: data.user.uid,
        nick: this.nick,
        email: this.email
      };
      this._us.createUser(user)
        .then((data2) => {
          alert('Registrado correctamente');
          console.log('registro correcto!', data2);
        }).catch((error) => {
          alert('Un error ha ocurrido');
          console.log('ocurrio un error ', error);
        });
    } catch (error_1) {
      alert('Un error ha ocurrido');
      console.log('ocurrio un error ', error_1);
    }
  }

  ngOnInit() {
  }

}
